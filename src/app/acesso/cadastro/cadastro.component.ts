import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Usuario } from 'src/app/shared/usuario.model';
import { AutenticacaoService } from 'src/app/autenticacao.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {

  @Output()
  public exibirPainel : EventEmitter<string> = new EventEmitter<string>();

  public formulario : FormGroup = new FormGroup({
    'email' : new FormControl(null),
    'nome_completo' : new FormControl(null),
    'nome_usuario' : new FormControl(null),
    'senha' : new FormControl(null)

  })

  constructor(
    private autenticasaoService : AutenticacaoService
  ) { }

  ngOnInit() {
  }

  public exibirPainelLogin() : void {
    this.exibirPainel.emit('login');
  }

  public cadastrarUsuario() : void {
    this.autenticasaoService.cadastrarUsuario(this.getUsuario())
                            .then(() => { this.exibirPainelLogin(); });
  }

  private getUsuario() : Usuario {
    return new Usuario(
      this.formulario.value.email,
      this.formulario.value.nome_completo,
      this.formulario.value.nome_usuario,
      this.formulario.value.senha
    )
  }

  get email(): any { return this.formulario.get('email'); }
  get nomeCompleto() : any { return this.formulario.get('nome_completo'); }
  get nomeUsuario() : any { return this.formulario.get('nome_usuario'); }
}
