import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

import { Imagem } from './imagem.model';


@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css'],
  animations: [
    trigger('banner',[
      state('escondido', style({
        opacity: 0
      })),
      state('visivel', style({
        opacity: 1
      })),
      transition('visivel <=> escondido', animate('1s ease-in'))
    ])
  ]
})
export class BannerComponent implements OnInit {

  public estado : string = 'visivel';
  
  public imagens : Array<Imagem> = [
    {estado : 'visivel', url : '/assets/banner-acesso/img_1.png'},
    {estado : 'escondido', url : '/assets/banner-acesso/img_2.png'},
    {estado : 'escondido', url : '/assets/banner-acesso/img_3.png'},
    {estado : 'escondido', url : '/assets/banner-acesso/img_4.png'},
    {estado : 'escondido', url : '/assets/banner-acesso/img_5.png'}
  ]

  constructor() { }

  ngOnInit() {
    setTimeout(() => this.logicaRotacao(), 2000);
  }

  private logicaRotacao() : void {
      let index : number

    //percorrer o array verificar se existe uma imagens a ser exibida
    for(let i = 0; i <= this.imagens.length; i++) {
      
      if(this.imagens[i].estado === 'visivel') {
        this.imagens[i].estado = 'escondido';       
        
        index = i === this.imagens.length - 1 ? 0 : i + 1;                
        break;
      }
      
    }

    this.imagens[index].estado = 'visivel';

    //continua executando a logicaRotação
    setTimeout(() => this.logicaRotacao(), 2000);
  }

 }
