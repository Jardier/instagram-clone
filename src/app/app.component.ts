import { Component, OnInit } from '@angular/core';

import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'instagram-clone';

  ngOnInit() {
     // Initialize Firebase
     var config = {
      apiKey: "AIzaSyCJE2bzAtvRqWqvFzmIP1KgO5NxM0RFFcI",
      authDomain: "instagram-clone-de92b.firebaseapp.com",
      databaseURL: "https://instagram-clone-de92b.firebaseio.com",
      projectId: "instagram-clone-de92b",
      storageBucket: "instagram-clone-de92b.appspot.com",
      messagingSenderId: "144695900234"
    };

    firebase.initializeApp(config);
  }
}
