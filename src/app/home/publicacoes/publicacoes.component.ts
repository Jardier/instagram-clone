import { Component, OnInit } from '@angular/core';
import  * as firebase from 'firebase';
import { BancoService } from '../../banco.service';

@Component({
  selector: 'app-publicacoes',
  templateUrl: './publicacoes.component.html',
  styleUrls: ['./publicacoes.component.css']
})
export class PublicacoesComponent implements OnInit {

  public email : string;
  public publicacoes : Array<any> [];

  constructor(
    private bancoService : BancoService
  ){}

  ngOnInit() {
     //recuperar o usuario logado
     firebase.auth()
     .onAuthStateChanged((usuer) => {
        this.email = usuer.email;
        this.atualizarTimeLine();   
      });
            
  }

  public atualizarTimeLine() : void {
    this.bancoService.buscarPublicacoes(this.email)
                     .then((resposta : any) => {
                        this.publicacoes = resposta;
                     });
  }


}
