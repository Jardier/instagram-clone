import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InserirPublicacoesComponent } from './inserir-publicacoes.component';

describe('InserirPublicacoesComponent', () => {
  let component: InserirPublicacoesComponent;
  let fixture: ComponentFixture<InserirPublicacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InserirPublicacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InserirPublicacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
