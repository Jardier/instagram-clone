import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import * as firebase from 'firebase';

import { BancoService } from 'src/app/banco.service';
import { ProgressoService } from 'src/app/progresso.service';

import { Observable, Subject } from 'rxjs';
import 'rxjs/Rx';


@Component({
  selector: 'app-inserir-publicacoes',
  templateUrl: './inserir-publicacoes.component.html',
  styleUrls: ['./inserir-publicacoes.component.css']
})
export class InserirPublicacoesComponent implements OnInit {

  public email : string;
  public imagem : any;
  public progressoStatus : string = 'pendente';
  public progressoPorcentagem : number;

  @Output()
  public atualizarTimeLine : EventEmitter<any> = new EventEmitter<any>();
 
  public formulario : FormGroup = new FormGroup({
    "titulo" : new FormControl(null)
  })

  constructor(
    private bancoService : BancoService,
    private progressoService : ProgressoService
  ){}

  ngOnInit() {
    //recuperar o usuario logado
    firebase.auth()
            .onAuthStateChanged((usuer) => {
      this.email = usuer.email;
    });
  }

  public publicar() : void {
    this.bancoService.publicar({
      'email' : this.email,
      'titulo' : this.formulario.value.titulo,
      'imagem' : this.imagem[0]
    });

    //criamos um obserble para acompanha o progresso
    let progressoUpload = Observable.interval(1500);

    let continua = new Subject();
    continua.next(true);
    
    progressoUpload
        .takeUntil(continua)
        .subscribe(() =>{
          
          this.progressoPorcentagem = 
            Math.round((this.progressoService.estado.bytesTransferred 
                      / this.progressoService.estado.totalBytes) * 100);

          this.progressoStatus = 'andamento';

          if(this.progressoService.status === 'concluído'){
            continua.next(false);
            this.progressoStatus = 'concluído';
            this.atualizarTimeLine.emit();
          }
    })
   
  }

  public preparaImagemUpload(event : Event) : void {
    this.imagem = (<HTMLInputElement>event.target).files;

  }

  public fecharModal() : void {
    this.formulario.reset();
    this.progressoStatus = 'pendente';
    
  }
     

}
