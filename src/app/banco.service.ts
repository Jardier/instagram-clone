import * as firebase from 'firebase';
import { Injectable } from '@angular/core';
import { ProgressoService } from './progresso.service';

@Injectable()
class BancoService {

    constructor(
        private progressoService : ProgressoService
    ){}

    public publicar(publicacao : any ) : void {
        
        firebase.database()
        .ref(`publicacoes/${btoa(publicacao.email)}`)
        .push(publicacao.titulo)
        .then((resposta : any) => {
            //chave associado a publicação do usuário 
            let nomeImagem = resposta.key; 
            
            firebase.storage()
                    .ref()
                    .child(`imagens/${nomeImagem}`)
                    .put(publicacao.imagem)
                    .on(firebase.storage.TaskEvent.STATE_CHANGED,
                            ((snapshot) => {
                               // console.log(snapshot);
                                this.progressoService.status = 'andamento';
                                this.progressoService.estado = snapshot;
                            }),
                            ((error) => {
                                this.progressoService.status = 'erro';
                            }),
                            () => {
                                this.progressoService.status = 'concluído';
                            }                    
                        );

        });
    }

    public buscarPublicacoes(email : string)  : Promise<any> {

        return new Promise((resolve,reject) => {
            let publicacoes : Array<any> = [];
            
            //buscar todas as publicações(database)
            firebase.database()
            .ref(`publicacoes/${btoa(email)}`)
            .orderByKey()
            .once('value')
            .then((snapshot: any) => {
                           
               snapshot.forEach((childSnapshot : any) => {
                    let publicacao : any = {}; 
                    publicacao.titulo = childSnapshot.val();
                    publicacao.key = childSnapshot.key;
                    //inserir a publicações na lista conforme está no database
                    publicacoes.push(publicacao);
                });      
                return publicacoes.reverse();
            })
            .then((publicacoes : any) => {
                
                publicacoes.forEach((publicacao : any) => {
                    //Buscar as imagens no Storage
                    firebase.storage()
                            .ref()
                            .child(`imagens/${publicacao.key}`)
                            .getDownloadURL()
                            .then((url : string) => {
                                publicacao.url = url;

                                //Buscar o nome do Usuário responsável pela publicação
                                firebase.database()
                                        .ref(`usuario_detalhe/${btoa(email)}`)
                                        .once('value')
                                        .then((snapshot : any) => {
                                           publicacao.nomeUsuario = snapshot.val().nomeUsuario;
                                        });
                            });                  
                });
            });
            
            resolve(publicacoes);    
        });
    }
}
export {BancoService}