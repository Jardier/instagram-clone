import { Usuario } from './shared/usuario.model';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

import * as firebase from 'firebase';

@Injectable()
export class AutenticacaoService {

    public tokenId : string;

    constructor(
        private router : Router
    ){}

    public cadastrarUsuario(usuario : Usuario) : Promise<any> {
        
       return firebase.auth()
                .createUserWithEmailAndPassword(usuario.email, usuario.senha)
                .then((resposta : any) => {
                    //removendo a senha do atributo senha do objeto usuário
                    delete usuario.senha;

                    //registrando o dados complementares do usuário no path email 
                    //btoa - criptografa base64 - atob, descriptografa 
                    firebase.database()
                            .ref(`usuario_detalhe/${btoa(usuario.email)}`)
                            .set(usuario);                    
                })
                .catch((erro : Error) => {
                    console.log(erro);
                })

    }

    public autenticar(email: string,senha : string) : void {

        firebase.auth()
                .signInWithEmailAndPassword(email,senha)
                .then((reposta) => {
                    
                    firebase.auth().currentUser
                            .getIdToken()
                            .then((idToken : string) => {
                                this.tokenId = idToken;
                                //adicionando o toke no localStorage()
                                localStorage.setItem('tokenId', this.tokenId);
                                //direcionando paro o home
                                this.router.navigate(['home']);
                            })
                            
                })
                .catch((erro : Error) => {
                    console.log('Ocorreu um erro: ', erro);
                })
    }

    public autenticado() : boolean {

        if(this.tokenId === undefined && localStorage.getItem('tokenId') !== null) {
            this.tokenId = localStorage.getItem('tokenId');
        }
        if(this.tokenId === undefined) {
            this.router.navigate(['/']);
        }
        return this.tokenId !== undefined;    
    }

    public sair() : void {
        firebase.auth()
                .signOut()
                .then(() => {
                    localStorage.removeItem('tokenId');
                    this.tokenId = undefined;
                    this.router.navigate(['/']);
                })
    }
}