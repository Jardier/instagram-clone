import { Component, OnInit, Input, ContentChild, AfterContentInit } from '@angular/core';
import { FormControlName } from '@angular/forms';


@Component({
  selector: 'app-input-container',
  templateUrl: './input-container.component.html',
  styleUrls: ['./input-container.component.css']
})
export class InputContainerComponent implements OnInit, AfterContentInit {

  public input : any;

  @Input()
  public errorMessage : string;

  @ContentChild(FormControlName)
  public formControl : FormControlName;

  constructor() { }

  ngOnInit() {
  }
  
  ngAfterContentInit(): void {
    this.input = this.formControl;

    if(this.input === undefined) {
      throw new Error("Esse componente precisa ser usado com uma diretiva formControlName");
    }
  }

  public hasSuccess() : boolean {
    return this.input.valid && (this.input.dirty || this.input.touched);
  }

  public hasError() : boolean {
    return this.input.invalid && (this.input.dirty || this.input.touched);
  }
}
